# 4.2 Celsius to Fahrenheit

## The problem
Take the temperature in degrees Celsius and convert it to Fahrenheit.

## Hins:
  To convert degrees Celsius temperature to Fahrenheit, you have to multiply by 9 and divide by 5.
   
   And then, add 32.


Think for a second...How will you multiply a variable by 9 and then divide by 5? and then add 32. Can you do it without looking at the solution? 

## The solution
```python
celsius = float(input("Enter temperature in degrees Celsius: "))
 
fahrenheit = celsius*9/5+32
 
print("Temperature in Fahrenheit:", fahrenheit)
```
 
**[Try it on Programming Hero](https://play.google.com/store/apps/details?id=com.learnprogramming.codecamp)**

&nbsp;
[![Next Page](../assets/next-button.png)](../README.md)
&nbsp;

###### tags: `programmig-hero` `python` `float` `int` `math`

